<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('product_code' , 14);
            $table->string('product_title',250);
            $table->string('product_slug',250);
            $table->integer('product_price');
            $table->integer('product_stock');
            $table->integer('product_discount');
            $table->tinyInteger('product_type');
            $table->integer('product_coupon_count');
            $table->text('product_description');
            $table->tinyInteger('product_status');
            $table->boolean('product_visible');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
