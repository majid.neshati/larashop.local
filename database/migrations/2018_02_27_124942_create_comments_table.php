<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {

            $table->increments('comment_id');

            $table->integer('comment_user_id');

            $table->integer('commentable_id');

            $table->string('commentable_type');

            $table->integer('comment_parent_id');

            $table->string('comment_body');

            $table->smallInteger('comment_source');

            $table->ipAddress('comment_ip');

            $table->tinyInteger('comment_approved')->default(0);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
