<?php
/**
 * Created by PhpStorm.
 * User: laravel
 * Date: 1/20/18
 * Time: 7:33 PM
 */

namespace app\Presenters;


class Currency
{

    public static function toman(int $amount)
    {
        return number_format($amount/10) . 'تومان';
    }

}