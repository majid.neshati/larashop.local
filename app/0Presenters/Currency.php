<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 2/6/18
 * Time: 10:50 PM
 */

namespace App\Presenters;


class Currency
{
    public static function toman(int $amount)
    {
        return number_format($amount). 'تومان';
    }

}