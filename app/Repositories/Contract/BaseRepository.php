<?php


namespace App\Repositories\Contract;


abstract class BaseRepository
{

    protected $model;

    public function __construct()
    {

    }

    public function find(int $id)
    {
        return $this->model::find($id);
    }

    public function all()
    {
        return $this->model::all();
    }

    public function create($data)
    {
        return $this->model::create($data);
    }
}