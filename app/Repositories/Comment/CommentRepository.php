<?php
/**
 * Created by PhpStorm.
 * User: majid
 * Date: 2/27/18
 * Time: 5:10 PM
 */

namespace App\Repositories\Comment;


use App\Models\Comment;
use App\Models\Product;
use App\Repositories\Contract\BaseRepository;

class CommentRepository extends BaseRepository
{
    protected $model = Comment::class;

    public function commentByModel(int $model_id,string $model_type = Product::class)
    {
        $model_object= $model_type::find($model_id);
        return $model_object->comments;
    }
}