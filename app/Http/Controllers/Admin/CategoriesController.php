<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class CategoriesController extends BaseController
{
    public function __construct()
    {
        parent::__construct(CategoryRepository::class);
    }

    public function index()
    {
        $title = " دسته بندی";
        $categories = $this->repository->all();
        return View('admin.category.index',compact('title', 'categories'));
    }

    public function create()
    {
        $title = "افزودن دسته بندی";
        $categories = $this->repository->all();
        return View('admin.category.create', compact('title', 'categories'));
    }

    public function store(Request $request)
    {

        $request->validate([

            'category_title' => 'required',
            'category_slug' => 'required',
            'category_parent_id' => 'required',

        ]);

        $newCategory = $this->repository->create([

            'category_title' => $request->input('category_title'),
            'category_slug' => $request->input('category_slug'),
            'category_parent_id' => $request->input('category_parent_id')

        ]);

        if ($newCategory && is_a($newCategory, Category::class)) {

            return redirect()->back()->with('status', 'دسته بندی جدید با موفقیت ذخیره شد .');
        }
    }
}
