<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\Types\Null_;

class BaseController extends Controller
{
    protected $repository;
    protected $data;

    public function __construct($repository = null)
    {
        $this->repository = !is_null($repository) ? new $repository : null ;
        $this->data=[];
    }
}
