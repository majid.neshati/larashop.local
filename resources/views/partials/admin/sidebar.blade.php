
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="جستجو">
                <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">منوی فروشگاه</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="active"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-users"></i> <span>داشبورد</span></a></li>

            <li class="treeview">
                <a href="#"><i class="fa fa-product-hunt"></i> <span>محصولات</span>
                    <span class="pull-left-container">
                <i class="fa fa-angle-right pull-left"></i>
              </span>
                </a>
                <ul class="treeview-menu">

                    <li><a href="{{ route('admin.products') }}"><i class="fa fa-circle-o"></i>همه محصولات</a></li>
                    <li><a href="{{ route('admin.products.create') }}"><i class="fa fa-circle-o"></i> افزودن محصول </a></li>
                    <li><a href="{{ route('admin.category')  }}"><i class="fa fa-circle-o"></i> دسته بندی محصول </a></li>
                    <li><a href="{{ route('admin.category.create') }}"><i class="fa fa-circle-o"></i>افزودن دسته بندی </a></li>

                </ul>
            </li>


            <li><a href="#{{-- route('admin.user')  --}}"><i class="fa fa-users"></i> <span>کاربران</span></a></li>

            <li><a href="{{ route('admin.comments')  }}"><i class="fa fa-comments"></i> <span> دیدگاه ها </span></a></li>

            
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>