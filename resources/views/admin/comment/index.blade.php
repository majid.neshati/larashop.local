@extends('layouts.admin')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">

                        @include('admin.comment.columns')

                        <tbody>
                           @if($comments && count($comments) > 0)
                                @foreach($comments as $comment)
                                    @include('admin.comment.item')
                                @endforeach
                            @else
                                @include('admin.comment.no-item')
                            @endif
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->


@endsection

