@extends('layouts.admin')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">

                        @include('admin.product.columns')

                        <tbody>
                           @if($products && count($products) > 0)
                                @foreach($products as $product)
                                    @include('admin.product.item')
                                @endforeach
                            @else
                                @include('admin.product.no-item')
                            @endif
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->


@endsection

