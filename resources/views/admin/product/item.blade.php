<tr>
    <td>{{ $product->product_code  }}</td>
    <td>{{ $product->product_title  }}</td>
    <td>{{ $product->product_price  }}</td>
    <td>{{ 0  }}</td>
    <td>{{ $product->product_stock  }}</td>
    <td>{{ $product->product_status  }}</td>
    <td class="text-center">
            <a href="{{ route('admin.comments').'?pid='.$product->product_id }}" class="btn btn-default bg-purple"><i class="fa fa-comments"></i></a>
            <a href="#" class="btn btn-default bg-orange"><i class="fa fa-edit"></i></a>
            <a href="#" class="btn btn-danger"><i class="fa fa-remove"></i></a>
    </td>