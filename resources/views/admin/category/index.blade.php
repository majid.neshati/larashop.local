@extends('layouts.admin')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">

                        @include('admin.category.columns')

                        <tbody>
                           @if($categories && count($categories) > 0)
                                @foreach($categories as $category)
                                    @include('admin.category.item')
                                @endforeach
                            @else
                                @include('admin.category.no-item')
                            @endif
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->


@endsection

