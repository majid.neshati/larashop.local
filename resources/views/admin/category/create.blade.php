@extends('layouts.admin')

@section('content')

    <div class="box box-primary container">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>

        @include('partials.admin.errors')

        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif

        <form method="post" action="{{ route('admin.category.store') }}">
            {{ csrf_field()  }}
            <div class="box-body">

                <div class="form-group">
                    <label>عنوان دسته بندی :</label>
                    <input
                            type="text"
                            name="category_title"
                            class="form-control">
                </div>

                <div class="form-group">
                    <label>نامک دسته بندی  :</label>
                    <input
                            type="text"
                            name="category_slug"
                            class="form-control">
                </div>

                <div class="form-group">
                    <label>والد :</label>
                    <select class="form-control" name="category_parent_id" id="category_parent_id">
                        @if( $categories && count($categories) > 0 )
                            <option value="0">بدون والد</option>
                            @foreach($categories as $category)
                                <option value="{{$category->category_id}}">{{$category->category_title}}</option>
                            @endforeach
                        @endif
                           {{-- <option value="0">بدون والد</option> --}}
                    </select>
                </div>

                <button type="submit" class="btn btn-fill btn-info">افزودن</button>

            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="card">

                <div class="content">

                </div> <!-- end card -->

            </div>
        </div>
@endsection