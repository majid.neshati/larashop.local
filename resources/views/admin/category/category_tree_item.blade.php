<li class="checkbox">
    <label>
        <input type="checkbox"
               id="{{ $loop->index }}"
               name="product_categories[]"
               value="{{ $category->category_id }}">
        {{ $category->category_title }}
    </label>



        @if(isset($categories[$category->category_id]))

            @include('admin.category.category_tree',['collection' => $categories[$category->category_id] ])

        @endif

</li>